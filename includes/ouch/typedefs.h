/*
 * typedefs.h
 *
 *  Created on: 21-Mar-2019
 *      Author: dhaval
 */

#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#include <boost/unordered_set.hpp>

typedef  boost::unordered_set<int> unordered_set;

// For strategy

#define BID 'B'
#define ASK 'S'
#define BEST 100
#define OPPOSITE 2000
#define OPPORTUNISTIC 3000

// Ouch message types
typedef char orderBookId_t[14];
typedef int price_t;
typedef uint64_t quantity_t;
typedef uint32_t symbol_t;
typedef uint8_t tif_t;
typedef uint8_t openOrClose_t;
typedef uint64_t display_t;
typedef uint8_t clientCategory_t;
typedef uint8_t offHours_t;
typedef char clientAccount_t[16];
typedef char customerInfo_t[15];
typedef char exchangeInfo_t[32];
typedef char side_t;

#endif /* TYPEDEFS_H_ */
