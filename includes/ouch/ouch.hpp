#ifndef OUCH_OUCH_H
#define OUCH_OUCH_H

#include <arpa/inet.h>
#include <fstream>
#include <cstring>
#include <iomanip>
#include <boost/format.hpp>
#include "typedefs.h"

namespace OUCH {
#ifndef packed
#define packed __attribute__ ((packed))
#endif

#define ntohll(y) (((uint64_t)ntohl(y)) << 32 | ntohl(y>>32)) // ! little-endian platform only
#define htonll(y) (((uint64_t)htonl(y)) << 32 | htonl(y>>32)) // ! little-endian platform only


static inline void rpadStr(char* dest, size_t destLen, const char* src,
		size_t srcLen) {
	strncpy(dest, src, destLen);
	if (srcLen < destLen)
		memset(dest + srcLen, ' ', destLen - srcLen);
}

static inline void rpadStr(char* dest, size_t destLen, const std::string& src) {
	rpadStr(dest, destLen, src.c_str(), src.size());
}

#define R_PAD_STR(dest, src) rpadStr(dest, sizeof(dest), src)

// all integer are unsigned big-endian
// alpha fileds are left-justified and padded on the right side with spaces

struct Message {
	Message(char type = ' ') :
			type(type) {
	}

	char type;
}packed;

static inline size_t lengthRTrim(const char* str, size_t n) {
	auto p = str + n;
	while (p > str) {
		if (*(--p) == ' ')
			n--;
		else
			break;
	}
	return n;
}

static inline char toOuchSide(char side) {
	switch (side) {
	case '1':
		return 'B';
		break;
	case '2':
		return 'S';
		break;
	case '5':
		return 'T';
		break;
	default:
		return side;
	}
}

static inline void writeSide(std::ostream& out, char side) {
	out << "54=";
	switch (side) {
	case 'B':
		out << '1';
		break;
	case 'S':
		out << '2';
		break;
	case 'T':
		out << '5';
		break;
	default:
		out << side;
		break;
	}
	out << '\1';
}

#define WRITE_RTRIM(out, str) out.write(str, lengthRTrim(str, sizeof(str)))

struct EnterOrderMessage: public Message {
	// Note: uint8_t is used since it should be exactly 1 byte. It is used as a stand-in for
	// the numeric datatype in the OUCH spec. uint64_t is used in the same way.
	static const char TYPE = 'O'; // 1 bytes
	orderBookId_t id; // 14 bytes
	symbol_t symbol; // 4 bytes
	side_t side; // 1 byte
	quantity_t quantity; // 8 bytes
	price_t price; // fixed point format, 4 decimal digits
	tif_t tif; // in seconds, 0: ioc, 99998: until market close, 99999: until end of day
	openOrClose_t openOrClose;
	clientAccount_t clientAccount; // DE-1 if Derivaties
	customerInfo_t customerInfo;
	exchangeInfo_t exchangeInfo; // Equity = 1, Derivatives = empty
	display_t display; // 0
	clientCategory_t clientCategory; // default 1
	offHours_t offHours; // 0
	char reserved[7];

	// CONSTRUCTORS
	EnterOrderMessage()
		: Message(TYPE) {}

	EnterOrderMessage(const std::string& _id, uint32_t symbol, char side,
			uint64_t quantity, int price) :
			Message(TYPE), symbol(symbol), side(side),  quantity(quantity), price(
					price), tif(0), openOrClose(0), clientAccount {}, customerInfo { }, exchangeInfo {'1'}, display(
					0), clientCategory(1), offHours(0) {

	    // memcpy(this->quantity, quantity, 8);
		this->quantity = quantity;
		R_PAD_STR(this->id, _id.c_str());
		R_PAD_STR(clientAccount, this->clientAccount);
		R_PAD_STR(customerInfo, this->customerInfo);
		R_PAD_STR(exchangeInfo, this->exchangeInfo);
	}

	EnterOrderMessage(const std::string& _id, uint32_t symbol, char side,
		uint64_t quantity, int price, tif_t tif, openOrClose_t openOrClose,
		const std::string clientAccount, const std::string customerInfo, const std::string exchangeInfo,
		display_t display, clientCategory_t clientCategory, offHours_t offHours) :
			Message(TYPE), symbol(symbol), side(side),  quantity(quantity), price(
			price), tif(tif), openOrClose(openOrClose), display(
			display), clientCategory(clientCategory), offHours(offHours) {

			// memcpy(this->quantity, quantity, 8);
			this->quantity = quantity;
			R_PAD_STR(this->id, _id.c_str());
			R_PAD_STR(this->clientAccount, clientAccount.c_str());
			R_PAD_STR(this->customerInfo, customerInfo.c_str());
			R_PAD_STR(this->exchangeInfo, exchangeInfo.c_str());
		}


	void write(std::ostream& out) {
		out << "TYPE: " << TYPE << ", " << "ID: " << id << ", " << "symbol: "
				<< symbol << ", " << "side: " << side << ", " << "quantity: "
				<< quantity << ", " << "price: " << price << ", "
				<< "tif: " << std::to_string(tif) << ", " << "openOrClose: "
				<< std::to_string(openOrClose) << ", " << "customerInfo: " << customerInfo
				<< ", " << "clientAccount: " << clientAccount << ", "
				<< "exchangeInfo: " << exchangeInfo << ", " << "display: "
				<< display << ", " << "clientCategory: " << std::to_string(clientCategory)
				<< ", " << "offHours: " << offHours << "\n";
	}

	std::string toString() {
			std::stringstream out;
			out << "TYPE: " << TYPE << ", " << "ID: " << id << ", " << "symbol: "
					<< symbol << ", " << "side: " << side << ", " << "quantity: "
					<< quantity << ", " << "price: " << price << ", "
					<< "tif: " << std::to_string(tif) << ", " << "openOrClose: "
					<< std::to_string(openOrClose) << ", " << "customerInfo: " << customerInfo
					<< ", " << "clientAccount: " << clientAccount << ", "
					<< "exchangeInfo: " << exchangeInfo << ", " << "display: "
					<< display << ", " << "clientCategory: " << std::to_string(clientCategory)
					<< ", " << "offHours: " << offHours << "\n";
			return out.str();
		}

	void hton() {
		symbol = htonl(symbol);
		quantity = htonll(quantity);
		price = htonl(price);
		tif = htonl(tif);
		offHours = htonl(offHours);
		display = htonl(display);

	}

	void ntoh() {
		symbol = ntohl(symbol);
		quantity = ntohll(quantity);
		price = ntohl(price);
		display = ntohl(display);
	}
}packed;
static_assert(sizeof(EnterOrderMessage)==114, "sizeof(EnterOrderMessage) != 114");

struct ReplaceOrderMessage: public Message {
	static const char TYPE = 'U';
	orderBookId_t existingOrderToken;
	orderBookId_t replacementOrderToken;
	quantity_t quantity; // 8 bytes
	price_t price;
	openOrClose_t openOrClose;
	clientAccount_t clientAccount;
	customerInfo_t customerInfo;
	exchangeInfo_t exchangeInfo;
	display_t display;
	clientCategory_t clientCategory;
	char reserved[8];

	ReplaceOrderMessage():
		Message(TYPE) {}

	ReplaceOrderMessage(const std::string& oldId, const std::string& newId,
			uint64_t quantity, int price) :
			Message(TYPE), existingOrderToken { }, replacementOrderToken { }, quantity(quantity), price(
					price), openOrClose(0), clientAccount { }, customerInfo { }, exchangeInfo { }, display {
					1 } {
		this->clientCategory = 1;
		R_PAD_STR(this->existingOrderToken, newId);
		R_PAD_STR(this->replacementOrderToken, newId);
		R_PAD_STR(this->clientAccount, clientAccount);
		R_PAD_STR(this->customerInfo, customerInfo);
		R_PAD_STR(this->exchangeInfo, exchangeInfo);
	}

	ReplaceOrderMessage(const std::string& oldId, const std::string& newId,
			uint64_t quantity, int price, const std::string clientAccount, const std::string customerInfo,
			const std::string exchangeInfo, display_t display, clientCategory_t clientCategory)
				: Message(TYPE), quantity(quantity), price(
				  price), openOrClose(0), display(display) {

			R_PAD_STR(this->existingOrderToken, newId);
			R_PAD_STR(this->replacementOrderToken, newId);
			R_PAD_STR(this->clientAccount, clientAccount);
			R_PAD_STR(this->customerInfo, customerInfo);
			R_PAD_STR(this->exchangeInfo, exchangeInfo);
		}

	void write(std::ostream& out) {
		out << "oldId: " << existingOrderToken << ", " << "newId: " << replacementOrderToken << ", "
				<< "price: " << price << ", " << "openOrClose: "
				<< openOrClose << ", " << "customerInfo: " << customerInfo
				<< ", " << "clientAccount: " << clientAccount << ", "
				<< "exchangeInfo: " << exchangeInfo << ", " << "display: "
				<< display << ", " << "clientCategory: " << clientCategory
				<< ", " << "\n";
	}

	std::string toString() {
		std::stringstream out;
		out << "TYPE: " << TYPE << "oldId: " << existingOrderToken << ", " << "newId: " << replacementOrderToken << ", "
			<< "price: " << price << ", " << "openOrClose: "
			<< openOrClose << ", " << "customerInfo: " << customerInfo
			<< ", " << "clientAccount: " << clientAccount << ", "
			<< "exchangeInfo: " << exchangeInfo << ", " << "display: "
			<< display << ", " << "clientCategory: " << clientCategory
			<< ", " << "\n";
		return out.str();
	}

	void hton() {
		quantity = htonll(quantity);
		price = htonl(price);
		display = htonl(display);

	}

	void ntoh() {
		quantity = ntohll(quantity);
		display = ntohl(display);
		price = ntohl(price);
	}
}packed;
static_assert(sizeof(ReplaceOrderMessage)==122, "sizeof(ReplaceOrderMessage) != 122");

struct CancelOrderMessage: public Message {
	static const char TYPE = 'X';
	orderBookId_t id;

	CancelOrderMessage(const orderBookId_t id) :
			Message(TYPE){
		R_PAD_STR(this->id, id);
	}

	void write(std::ostream& out) {
		out << "id: " << id << "\n";
	}

	std::string toString() {
		std::stringstream out;
		out << "TYPE: " << TYPE << ", " << "id: " << id << "\n";
		return out.str();
	}

	void hton() {
		// Does nothing because no numeric fields to convert
	}

	void ntoh() {
		// Does nothing because no numeric fields to convert
	}
}packed;
static_assert(sizeof(CancelOrderMessage) == 15, "sizeof(CancelOrderMessage) != 15");

struct CancelByOrderIdMessage: public Message {
	static const char TYPE = 'Y';
	symbol_t symbol;
	side_t side;
	uint64_t orderId;

	CancelByOrderIdMessage()
		: Message(TYPE) {}

	CancelByOrderIdMessage(uint32_t symbol, char side, uint64_t orderId) :
			Message(TYPE), symbol(symbol), side(side), orderId(orderId) {
		// R_PAD_STR(this->orderId, orderId);
	}

	void write(std::ostream& out) {
		out << "symbol: " << symbol << ", " << "side: " << side << ", "
				<< "orderId: " << orderId << "\n";
	}

	std::string toString() {
		std::stringstream out;
		out << "TYPE:" << TYPE << ", " << "symbol: " << symbol << ", " << "side: " << side << ", "
			<< "orderId: " << orderId << "\n";
		return out.str();
	}

	void hton() {
		symbol = htonl(symbol);
	}

	void ntoh() {
		symbol = ntohl(symbol);
	}
} packed;
static_assert(sizeof(CancelByOrderIdMessage) == 14, "sizeof(CancelOrderMessage) != 14");


/****************************************
 * OUTBOUND MESSAGES (sent from the server)
 ****************************************/

struct OrderAcceptedMessage: public Message {
	static const char TYPE = 'A';
	uint64_t tm;
	orderBookId_t orderToken;
	symbol_t symbol;
	side_t side; // B: buy, S: sell, T: sell short, E: sell short exempt
	uint64_t orderId; // 4 bytes.
	quantity_t quantity;
	price_t price;
	tif_t tif;
	openOrClose_t openOrClose;
	clientAccount_t clientAccount; // DE-1 if Derivaties
	uint8_t orderState;
	customerInfo_t customerInfo;
	exchangeInfo_t exchangeInfo; // Equity = 1, Derivatives = empty
	uint64_t perTradeQuantity; // 0
	display_t display; // 0
	clientCategory_t clientCategory; // default 1
	offHours_t offHours; // 0
	char reserved[3];

	OrderAcceptedMessage()
		: Message(TYPE) {}

	OrderAcceptedMessage(const EnterOrderMessage& o) :
			Message(TYPE), tm(0), symbol(o.symbol), side(o.side), quantity(o.quantity), price(o.price), tif(o.tif) {
		this->openOrClose = o.openOrClose;
		orderState = 0;
		strncpy(customerInfo, o.customerInfo, sizeof(o.customerInfo));
		strncpy(exchangeInfo, o.exchangeInfo, sizeof(o.exchangeInfo));

		this->perTradeQuantity = 0;
		display = o.display;
		clientCategory = o.clientCategory;
		offHours = o.offHours;
		strncpy(this->orderToken, o.id, sizeof(o.id));
		strncpy(this->customerInfo, o.customerInfo, sizeof(o.customerInfo));
		strncpy(this->exchangeInfo, o.exchangeInfo, sizeof(o.exchangeInfo));
		strncpy(this->clientAccount, o.clientAccount, sizeof(o.clientAccount));
	}

	bool isDead() const {
		return orderState == 'D';
	}

	void write(std::ostream& out) {
		out << "TYPE: " << TYPE << ", " << "id: " << orderToken << ", " << "symbol: "
				<< symbol << ", " << "side: " << side << ", " << "price: "
				<< price << ", "
				/* Note: std::hex may be slow */
				<< "quantity (hex): " << std::hex << quantity << ", "
				<< "tif: " << tif << ", "
				<< "openOrClose: " << openOrClose << ", " << "customerInfo: "
				<< customerInfo << ", " << "clientAccount: " << clientAccount
				<< ", " << "exchangeInfo: " << exchangeInfo << ", "
				<< "display: " << display << ", " << "clientCategory: "
				<< clientCategory << ", " << "offHours: " << offHours << "\n";
	}
	std::string toString() {
		std::stringstream out;
		out << "TYPE: " << TYPE << ", " << "id: " << orderToken << ", " << "symbol: "
				<< symbol << ", " << "side: " << side << ", " << "price: "
				<< price << ", "
				/* Note: std::hex may be slow */
				<< "quantity (hex): " << std::hex << quantity << ", "
				<< "tif: " << tif << ", "
				<< "openOrClose: " << openOrClose << ", " << "customerInfo: "
				<< customerInfo << ", " << "clientAccount: " << clientAccount
				<< ", " << "exchangeInfo: " << exchangeInfo << ", "
				<< "display: " << display << ", " << "clientCategory: "
				<< clientCategory << ", " << "offHours: " << offHours << "\n";
		return out.str();
	}
	void ntoh() {
		tm = ntohll(tm);
		symbol = ntohl(symbol);
		orderId = ntohll(orderId);
		quantity = ntohll(quantity);
		price = ntohl(price);
		perTradeQuantity = ntohl(perTradeQuantity);
		display = ntohl(display);
		orderId = ntohl(orderId);
		tif = ntohl(tif);
	}

	void hton() {
		tm = htonll(tm);
		orderId = htonl(orderId);
		quantity = htonl(quantity);
		price = htonl(price);
		display = htonl(display);
		perTradeQuantity = htonl(perTradeQuantity);
		symbol = htonl(symbol);
	}
}packed;
static_assert(sizeof(OrderAcceptedMessage) == 135, "sizeof(AcceptedMsg)!=135 ");


struct OrderRejectedMessage: public Message {
	static const char TYPE = 'J';
	uint64_t tm;
	char id[14];
	int reason;

	void write(std::ostream& out) {
		out << "TYPE: " << TYPE << ", " << "id: " << id << ", " << "reason: "
				<< reason << "\n";

	}
	std::string toString() {
		std::stringstream out;
		out << "TYPE: " << TYPE << ", " << "id: " << id << ", " << "reason: "
				<< reason << "\n";
		return out.str();

	}
	void ntoh() {
		tm = ntohll(tm);
		reason = ntohl(reason);
	}
}packed;
static_assert(sizeof(OrderRejectedMessage) == 27, "sizeof(OrderRejectedMessage) != 27");


struct OrderReplacedMessage: public Message {
	static const char TYPE = 'U';
	uint64_t tm;
	char replacementOrderToken[14];
	char previousOrderToken[14];
	symbol_t orderBookId;
	side_t side;
	uint64_t orderId; // The identifier assigned to the new order. Note that the number is only unique per Order book and side
	quantity_t quantity;
	price_t price;
	tif_t tif;
	openOrClose_t openOrClose;
	clientAccount_t clientAccount;
	uint8_t orderState;
	customerInfo_t customerInfo;
	exchangeInfo_t exchangeInfo; // Equity = 1, Derivatives = empty
	quantity_t perTradeQuantity;
	display_t display; // 0
	clientCategory_t clientCategory; // default 1

	bool isDead() const {
		return orderState == 'D';
	}

	void write(std::ostream& out) {
		out << "TYPE: " << TYPE << ", " << "oldId" << previousOrderToken << ", " << "newId: "
				<< replacementOrderToken << ", " << "symbol: " << orderBookId << ", " << "side: "
				<< side << ", " /* Note: std::hex may be slow */
				<< "quantity (hex): " << std::hex << quantity << ", " << "tif: "
				<< tif << ", " << "openOrClose: " << openOrClose << ", "
				<< "clientAccount: " << clientAccount << ", " << "orderState: "
				<< orderState << ", " << "customerInfo: " << customerInfo
				<< ", " << "exchangeInfo: " << exchangeInfo << ", "
				<< "perTradeQuantity: " << perTradeQuantity << ", "
				<< "display: " << display << ", " << "clientCategory: "
				<< clientCategory << "\n";
	}

	std::string write() {
		std::stringstream out;
		out << "TYPE: " << TYPE << ", " << "oldId" << previousOrderToken << ", " << "newId: "
				<< replacementOrderToken << ", " << "symbol: " << orderBookId << ", " << "side: "
				<< side << ", " /* Note: std::hex may be slow */
				<< "quantity (hex): " << std::hex << quantity << ", " << "tif: "
				<< tif << ", " << "openOrClose: " << openOrClose << ", "
				<< "clientAccount: " << clientAccount << ", " << "orderState: "
				<< orderState << ", " << "customerInfo: " << customerInfo
				<< ", " << "exchangeInfo: " << exchangeInfo << ", "
				<< "perTradeQuantity: " << perTradeQuantity << ", "
				<< "display: " << display << ", " << "clientCategory: "
				<< clientCategory << "\n";
		return out.str();
	}

	void hton() {
		// Does nothing because we will never send this message.
	}

	void ntoh() {
		tm = ntohll(tm);
		quantity = ntohll(quantity);
		price = ntohl(price);
		perTradeQuantity = ntohll(perTradeQuantity);
		display = ntohl(display);
		orderBookId = ntohl(orderBookId);
	}
}packed;
static_assert(sizeof(OrderReplacedMessage) == 145, "sizeof(OrderReplacedMessage) != 145");

struct OrderCanceledMessage: public Message {
	static const char TYPE = 'C';
	uint64_t tm;
	orderBookId_t id;
	symbol_t symbol;
	side_t side;
	uint32_t orderId;
	char reason;

	OrderCanceledMessage(const CancelOrderMessage& o) :
			Message(TYPE), tm(0), reason(' ') {
		memcpy(id, o.id, sizeof(o.id));
	}

	void write(std::ostream& out) {
		out << "TYPE: " << TYPE << ", " << "id: " << id << ", " << "tm: " << tm
				<< ", " << "symbol: " << symbol << ", " << "side: " << side
				<< ", " << "orderId: " << orderId << ", " << "reason: "
				<< reason << "\n";
	}

	std::string toString() {
		std::stringstream out;
		out << "TYPE: " << TYPE << ", " << "id: " << id << ", " << "tm: " << tm
				<< ", " << "symbol: " << symbol << ", " << "side: " << side
				<< ", " << "orderId: " << orderId << ", " << "reason: "
				<< reason << "\n";
		return out.str();
	}

	void ntoh() {
		tm = ntohll(tm);
	}

	void hton() {
		tm = htonll(tm);
		symbol = htonl(symbol);

	}

}packed;
// static_assert(sizeof(CanceledMsg)==28, "sizeof(CanceledMsg)!=28");

struct OrderExecutedMessage: public Message {
	static const char TYPE = 'E';
	uint64_t tm;
	orderBookId_t orderToken;
	symbol_t orderBookId; //Needed for Combination fills (Order Executed sent per leg).

	quantity_t tradedQuantity;
	price_t tradedPrice;
	uint8_t matchId[12]; // technically this is a Numeric type but we use a uint8_t since we don't have a 12 byte type
	clientCategory_t clientCategory;
	uint8_t reserved[16];

	void write(std::ostream& out) {
		out << "TYPE: " << TYPE << ", " << "orderToken: " << orderToken << ", " << "orderBookId: "
				<< orderBookId << ", " << "tradedQuantity: " << tradedQuantity
				<< ", " << "tradedPrice: " << tradedPrice << ", " << "matchId: "
				<< matchId << ", " << "clientCategory: " << clientCategory
				<< ", " << "\n";
	}

	std::string toString() {
		std::stringstream out;
		out << "TYPE: " << TYPE << ", " << "orderToken: " << orderToken << ", " << "orderBookId: "
				<< orderBookId << ", " << "tradedQuantity: " << tradedQuantity
				<< ", " << "tradedPrice: " << tradedPrice << ", " << "matchId: "
				<< matchId << ", " << "clientCategory: " << clientCategory
				<< ", " << "\n";
		return out.str();
	}

	void ntoh() {
		tm = ntohll(tm);
		tradedQuantity = ntohll(tradedQuantity);
		tradedPrice = ntohl(tradedPrice);
		orderBookId = ntohl(orderBookId);
		// Note: this might have to fixed later.

		// matchId = ntohll(matchId);

		// execShares = ntohl(execShares);
		// execPx = ntohl(execPx);
		// matchNum = ntohll(matchNum);
	}
} packed;
static_assert(sizeof(OrderExecutedMessage)==68, "sizeof(ExecMsg)!=68");

} // OUCH
#endif
