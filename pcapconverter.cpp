/*
 * pcapconverter.cpp
 *
 *  Created on: 03-May-2019
 *      Author: xhimanshuz
 */


#include<string>
#include<iostream>
#include<pcap.h>
#include"ouch/ouch.hpp"
#include<boost/endian/conversion.hpp>

using namespace OUCH;

void orderMsg(u_char *data)
{
	// Putting binary data to EnterOrderMessage Struct
	auto eom = (reinterpret_cast<OUCH::EnterOrderMessage*>(data));
	printf("\nEnter Order Message\nType: %c \nOrder Token: %s \nOrder Book ID: %d \nSide: %c \nQuantity: %d \nPrice: %d\nTime in Force: %d"
			"\nOpen Close: %d \nClient/Account: %s \nCustomer Info: %s \nExchange Info: %s \nDisplay Quantity: %d \n"
			"Client Category: %d \nOffHours: %d\n", eom->type, eom->id, boost::endian::endian_reverse(eom->symbol), eom->side, boost::endian::endian_reverse(eom->quantity), boost::endian::endian_reverse(eom->price),
			boost::endian::endian_reverse(eom->tif), boost::endian::endian_reverse(eom->openOrClose), eom->clientAccount, eom->customerInfo, eom->exchangeInfo, boost::endian::endian_reverse(eom->display), boost::endian::endian_reverse(eom->clientCategory), boost::endian::endian_reverse(eom->offHours));

}

void replaceMsg(u_char *data)
{
	auto eom = (reinterpret_cast<OUCH::ReplaceOrderMessage*>(data));
	printf("\nReplacement Order Message\nType: %c \nExisting Order: %s \nReplacement order Token: %s \nQuantity: %d \nPrice: %d"
				"\nOpen Close: %d \nClient/Account: %s \nCustomer Info: %s \nExchange Info: %s \nDisplay Quantity: %d \n"
				"Client Category: %d\n", eom->type, eom->existingOrderToken, eom->replacementOrderToken, boost::endian::endian_reverse(eom->quantity), boost::endian::endian_reverse(eom->price),
				boost::endian::endian_reverse(eom->openOrClose), eom->clientAccount, eom->customerInfo, eom->exchangeInfo, boost::endian::endian_reverse(eom->display), boost::endian::endian_reverse(eom->clientCategory));
}

int main(int argc, char **argv)
{
//    if(argc!=2)
//    {
//            std::cerr<<"Invalid PCAP file argument\n";
//            std::cerr<<"a.out fileUrl\n";
//            return 1;
//    }

	char *file = "addReplace.pcapng"; // Contain only add and replace data
//    char* file = "pcap.pcapng";			// default file

    std::cout<<"\nFile: "<< file<<std::endl;

    char errbuf[PCAP_ERRBUF_SIZE];

    //Open pcap file
    auto *handler = pcap_open_offline(file, errbuf);
    if(handler == NULL)
    {
        std::cerr<< "\nError in Handler!\n";
        return 2;
    }
    // Packet header
    struct  pcap_pkthdr *header;

    const u_char *data;

    u_int packectCount = 0;
    while(int returnValue = pcap_next_ex(handler, &header, &data)>=0)
    {
        if(header->len < 100)
            continue;
        auto msg = new OUCH::Message(data[55]);
        switch (msg->type)
        {
        case 'O':
        	std::cout<<"\n Enter Order Size: " << sizeof(OUCH::EnterOrderMessage)<<std::endl;
        	u_char data2[sizeof(OUCH::EnterOrderMessage)];
			std::copy(data+55, data+(55+sizeof(OUCH::EnterOrderMessage)), data2);
			orderMsg(data2);
			break;
        case 'U':
//                	std::cout<<"\n Replace Order Size: " << sizeof(OUCH::ReplaceOrderMessage)<<std::endl;
                	u_char rData[sizeof(OUCH::ReplaceOrderMessage)];
        			std::copy(data+55, data+(55+sizeof(OUCH::ReplaceOrderMessage)), rData);
        			replaceMsg(rData);
        			break;
        }
//
//        printf("Packet # %i \n", ++packectCount);
//        printf("Packet Size: %d bytes\n", header->len);
//        printf("Epoch Time: %d:%d seconds\n", header->ts.tv_sec, header->ts.tv_usec);
//        printf("Non Physical Packets: %d\n", (header->len)-52);

        printf("\n----------------\n");
    }
}


